package Utils;

import org.testng.Reporter;

public class LogUtils {

    public static void log(String log) {
        Reporter.log("<p>" + log + "</p>");
    }

    public static void log(String logMessage, String parameter) {
        Reporter.log("<p>" + logMessage + "<b>" + parameter + "</b>");
    }
}
