package Utils;

import org.openqa.selenium.Cookie;

import java.io.*;
import java.util.Date;
import java.util.StringTokenizer;

import Driver.DriverManager;

public class CookieHandling {

    private static final String pathToCookies = "./src/main/resources/cache/";
    private static final String pathToCombinedCookies = "./src/main/resources/cacheTest/";

    public static void SaveCookies(String userName)
    {
        File file = new File(getFileName(userName));
        try
        {
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter bWrite = new BufferedWriter(fileWrite);

            for(Cookie ck : DriverManager.getDriver().manage().getCookies())
            {
                bWrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                bWrite.newLine();
            }
            bWrite.close();
            fileWrite.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void SaveCombinedCookies(String userName)
    {
        File file = new File(getCombinedCookieFileName(userName));
        try
        {
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter bWrite = new BufferedWriter(fileWrite);
            bWrite.write("User '" + userName + "' saved cookies on the Login Page.");
            bWrite.write("\n====================================================\n");

            for(Cookie ck : DriverManager.getDriver().manage().getCookies())
            {
                bWrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                bWrite.newLine();
            }
            bWrite.close();
            fileWrite.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void AppendCombinedCookies(String userName)
    {
        File file = new File(getCombinedCookieFileName(userName));
        try
        {
            FileWriter fileWrite = new FileWriter(file, true);
            BufferedWriter bWrite = new BufferedWriter(fileWrite);
            bWrite.write("\nUser '" + userName + "' saved cookies on the Dashboard Page.");
            bWrite.write("\n====================================================\n");

            for(Cookie ck : DriverManager.getDriver().manage().getCookies())
            {
                bWrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                bWrite.newLine();
            }
            bWrite.close();
            fileWrite.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void ReadCookies(String userName)
    {
        File file = new File(getFileName(userName));
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader buffReader = new BufferedReader(fileReader);
            String strLine;
            while((strLine=buffReader.readLine())!=null){
                StringTokenizer token = new StringTokenizer(strLine,";");
                while(token.hasMoreTokens()){
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;

                    String val;
                    if(!(val=token.nextToken()).equals("null"))
                        expiry = new Date(val);
                    boolean isSecure = Boolean.parseBoolean(token.nextToken());
                    Cookie ck = new Cookie(name, value, domain, path, expiry, isSecure);
                    DriverManager.getDriver().manage().addCookie(ck);
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private static String getFileName(String userName)
    {
        return pathToCookies + userName.split("@")[0] + ".data";
    }

    private static String getCombinedCookieFileName(String userName)
    {
        return pathToCombinedCookies + userName.split("@")[0] + ".data";
    }
}
