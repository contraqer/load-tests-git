package Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public static WebDriver getDriver() {
        return driver.get();
    }

    public static void createInstance() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--window-size=2560,1440");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver webDriver = new ChromeDriver(options);

        //set instance of Chrome driver for each thread
        driver.set(webDriver);
        driver.get().manage().window().maximize();
        driver.get().manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.get().manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    public static void killDriver(){
        driver.get().quit();
        driver.remove();
    }
}
