package Pages;

import Utils.CookieHandling;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static Utils.LogUtils.log;

public class DashboardPage extends BasePage{

    @FindBy(how = How.CSS, using = "a[title='Log out']")
    public WebElement logOutLink;

    public LoginPage doLogout(String userName)
    {
        Assert.assertEquals(getPageTitle(), "Dashboard - Contraqer", "User is not on Dashboard page!");
        log("User is on page: 'Dashboard Page' where Server name is: ", getServerName());
        CookieHandling.SaveCookies(userName);
        logOutLink.click();
        waitForJQuery();
        log("User is again on page: 'Login page' after log off where Server name is: ", getServerName());
        return new LoginPage();
    }

    public void stayOnPageAndWriteToLog()
    {
        Assert.assertEquals(getPageTitle(), "Dashboard - Contraqer", "User is not on Dashboard page!");
        log("User is on page: 'Dashboard Page' where Server name is: ", getServerName());
        log("User does not perform 'log off' action.");
    }
}
