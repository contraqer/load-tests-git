package Pages;

import Driver.DriverManager;
import Utils.CookieHandling;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static Utils.LogUtils.log;

public class LoginPage extends BasePage{

    private DashboardPage dashboard;

    @FindBy(how = How.ID, using = "UserName")
    public WebElement loginBox;

    @FindBy(how = How.ID, using = "Password")
    public WebElement passwordBox;

    @FindBy(how = How.CSS, using = "button[title='Submit']")
    public WebElement submitButton;

    public DashboardPage doLogin(String userName, String userPassword)
    {
        waitForJQuery();
        Assert.assertEquals(getPageTitle(), "Log in - Contraqer", "User is not on Login page!");
        log("User is on page: 'Login Page' where Server name is: ", getServerName());
        loginBox.sendKeys(userName);
        passwordBox.sendKeys(userPassword);
        submitButton.click();
        waitForJQuery();
        return PageFactory.initElements(DriverManager.getDriver(), DashboardPage.class);
    }

    public void doLoginWithoutLogout(String userName, String userPassword)
    {
        waitForJQuery();
        Assert.assertEquals(getPageTitle(), "Log in - Contraqer", "User is not on Login page!");
        log("User is on page: 'Login Page' where Server name is: ", getServerName());
        loginBox.sendKeys(userName);
        passwordBox.sendKeys(userPassword);
        submitButton.click();
        waitForJQuery();
        dashboard = PageFactory.initElements(DriverManager.getDriver(), DashboardPage.class);
        dashboard.stayOnPageAndWriteToLog();
        CookieHandling.SaveCookies(userName);
    }

    public void doLoginWithoutLogoutAndSaveEachCookie(String userName, String userPassword)
    {
        waitForJQuery();
        Assert.assertEquals(getPageTitle(), "Log in - Contraqer", "User is not on Login page!");
        log("User is on page: 'Login Page' where Server name is: ", getServerName());
        CookieHandling.SaveCombinedCookies(userName);
        loginBox.sendKeys(userName);
        passwordBox.sendKeys(userPassword);
        submitButton.click();
        waitForJQuery();
        dashboard = PageFactory.initElements(DriverManager.getDriver(), DashboardPage.class);
        dashboard.stayOnPageAndWriteToLog();
        CookieHandling.SaveCookies(userName);
        CookieHandling.AppendCombinedCookies(userName);
    }

    public DashboardPage doLoginWithSavedCookies(String userName, String url)
    {
        waitForJQuery();
        CookieHandling.ReadCookies(userName);
        DriverManager.getDriver().get(url);
        waitForJQuery();
        Assert.assertEquals(getPageTitle(), "Dashboard - Contraqer", "User is not on Dashboard page!");
        return PageFactory.initElements(DriverManager.getDriver(), DashboardPage.class);
    }
}
