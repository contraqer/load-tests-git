package Pages;

import Driver.DriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    @FindBy(how = How.ID, using = "lblServerName")
    public WebElement labelServerName;

    protected String getServerName()
    {
        return labelServerName.getText().trim();
    }

    protected String getPageTitle()
    {
        return DriverManager.getDriver().getTitle();
    }

    protected void waitForJQuery() {
        (new WebDriverWait(DriverManager.getDriver(), 120)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(org.openqa.selenium.WebDriver d) {
                JavascriptExecutor js = (JavascriptExecutor) d;
                return (Boolean) js.executeScript("return (typeof jQuery == 'undefined') || jQuery.active == 0");
            }
        });
    }
}