package TestUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CsvDataReader {

    private static final String csvFile = "./src/test/resources/users.csv";
    private static final String csvSplitter = ",";

    public static UserCredentials GetUserCredentials(String userId) {
        UserCredentials userCredentials = new UserCredentials();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            while (( line = br.readLine()) != null) {
                if(line.contains(userId)){
                    String[] user = line.split(csvSplitter);
                    userCredentials.setUserName(user[1]);
                    userCredentials.setUserPassword(user[2]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userCredentials;
    }
}
