package LoginTest;

import BaseTest.BaseTest;
import Pages.DashboardPage;
import TestUtils.CsvDataReader;
import TestUtils.UserCredentials;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static Utils.LogUtils.log;

public class LoginWithCookiesTest extends BaseTest {

    @DataProvider(name = "userCredentialsDataProvider2", parallel = true )
    public static Object[][] userCredentials()
    {
        UserCredentials user1 = CsvDataReader.GetUserCredentials("user1");
        UserCredentials user2 = CsvDataReader.GetUserCredentials("user2");
        UserCredentials user3 = CsvDataReader.GetUserCredentials("user3");
        UserCredentials user4 = CsvDataReader.GetUserCredentials("user4");
        UserCredentials user5 = CsvDataReader.GetUserCredentials("user5");
        UserCredentials user6 = CsvDataReader.GetUserCredentials("user6");
        UserCredentials user7 = CsvDataReader.GetUserCredentials("user7");
        UserCredentials user8 = CsvDataReader.GetUserCredentials("user8");
        UserCredentials user9 = CsvDataReader.GetUserCredentials("user9");
        UserCredentials user10 = CsvDataReader.GetUserCredentials("user10");

        return new Object[][]{
                new Object[]{user1.getUserName()},
                new Object[]{user2.getUserName()},
                new Object[]{user3.getUserName()},
                new Object[]{user4.getUserName()},
                new Object[]{user5.getUserName()},
                new Object[]{user6.getUserName()},
                new Object[]{user7.getUserName()},
                new Object[]{user8.getUserName()},
                new Object[]{user9.getUserName()},
                new Object[]{user10.getUserName()}
        };
    }

    @Test(dataProvider = "userCredentialsDataProvider2")
    public void performLoginWithSavedCookies(String userName) {
        log("Trying to navigate directly to Dashboard with saved cookies as user: ", userName);
        DashboardPage dashboard = landingPage.doLoginWithSavedCookies(userName, baseUrl);
        dashboard.doLogout(userName);
    }
}
