package LoginTest;

import BaseTest.BaseTest;
import Pages.DashboardPage;
import TestUtils.CsvDataReader;
import TestUtils.UserCredentials;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static Utils.LogUtils.log;

public class LoginLogoutWithoutCookiesTest extends BaseTest {

    @DataProvider(name = "userCredentialsDataProvider", parallel = true )
    public static Object[][] userCredentials()
    {
        UserCredentials user1 = CsvDataReader.GetUserCredentials("user1");
        UserCredentials user2 = CsvDataReader.GetUserCredentials("user2");
        UserCredentials user3 = CsvDataReader.GetUserCredentials("user3");
        UserCredentials user4 = CsvDataReader.GetUserCredentials("user4");
        UserCredentials user5 = CsvDataReader.GetUserCredentials("user5");
        UserCredentials user6 = CsvDataReader.GetUserCredentials("user6");
        UserCredentials user7 = CsvDataReader.GetUserCredentials("user7");
        UserCredentials user8 = CsvDataReader.GetUserCredentials("user8");
        UserCredentials user9 = CsvDataReader.GetUserCredentials("user9");
        UserCredentials user10 = CsvDataReader.GetUserCredentials("user10");

        return new Object[][]{
                new Object[]{user1.getUserName(), user1.getUserPassword()},
                new Object[]{user2.getUserName(), user2.getUserPassword()},
                new Object[]{user3.getUserName(), user3.getUserPassword()},
                new Object[]{user4.getUserName(), user4.getUserPassword()},
                new Object[]{user5.getUserName(), user5.getUserPassword()},
                new Object[]{user6.getUserName(), user6.getUserPassword()},
                new Object[]{user7.getUserName(), user7.getUserPassword()},
                new Object[]{user8.getUserName(), user8.getUserPassword()},
                new Object[]{user9.getUserName(), user9.getUserPassword()},
                new Object[]{user10.getUserName(), user10.getUserPassword()}
        };
    }

    @Test(dataProvider = "userCredentialsDataProvider")
    public void performLoginAndSaveCookies(String userName, String userPassword) {
        log("Trying to log in as user: ", userName);
        DashboardPage dashboard = landingPage.doLogin(userName, userPassword);
        dashboard.doLogout(userName);
    }
}
