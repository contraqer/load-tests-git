package LoginTest;

import BaseTest.BaseTest;
import org.testng.annotations.Test;

import static Utils.LogUtils.log;

public class LoginOnlyWithoutCookiesSaveForEachPageTest extends BaseTest {

    @Test(dataProvider = "userCredentialsDataProvider", dataProviderClass = LoginLogoutWithoutCookiesTest.class)
    public void performLoginAndSaveEachPageCookie(String userName, String userPassword) {
        log("Trying to log in as user: ", userName);
        landingPage.doLoginWithoutLogoutAndSaveEachCookie(userName, userPassword);
    }
}