package LoginTest;

import BaseTest.BaseTest;
import org.testng.annotations.Test;
import static Utils.LogUtils.log;

public class LoginOnlyWithoutCookiesTest extends BaseTest {

    @Test(dataProvider = "userCredentialsDataProvider", dataProviderClass = LoginLogoutWithoutCookiesTest.class)
    public void performLoginAndSaveCookies(String userName, String userPassword) {
        log("Trying to log in as user: ", userName);
        landingPage.doLoginWithoutLogout(userName, userPassword);
    }
}
