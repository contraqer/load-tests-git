package BaseTest;

import Driver.DriverManager;
import Pages.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected LoginPage landingPage;
    public static String baseUrl = "";
    private static final String qaUrl = "https://qa.contraqer.com/";
    private static final String qaAutoUrl = "https://contraqerqaauto.azurewebsites.net/";
    private static final String prodUrl = "https://app.contraqer.com/";

    @BeforeMethod
    @Parameters({ "environment", "cookiesEnabled" })
    public void setup(String environment, String cookiesEnabled) {
        this.setLoginUrl(environment, cookiesEnabled);
        DriverManager.createInstance();
        DriverManager.getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        DriverManager.getDriver().get(baseUrl);
        landingPage = PageFactory.initElements(DriverManager.getDriver(), LoginPage.class);
    }

    @AfterMethod
    public void tearDown() {
        DriverManager.killDriver();
    }

    public void setLoginUrl(String environment, String cookiesEnabled)
    {
        if ("qa_auto".equals(environment)){
            baseUrl = qaAutoUrl;
            this.appendBaseUrlSuffix(cookiesEnabled);
        } else if ("qa".equals(environment)) {
            baseUrl = qaUrl;
            this.appendBaseUrlSuffix(cookiesEnabled);
        } else if ("prod".equals(environment)){
            baseUrl = prodUrl;
            this.appendBaseUrlSuffix(cookiesEnabled);
        }
    }

    private void appendBaseUrlSuffix(String cookiesEnabled)
    {
        if ("false".equals(cookiesEnabled)) {
            baseUrl += "Account/Login";
        } else if ("true".equals(cookiesEnabled)) {
            baseUrl += "Dashboard";
        }
    }
}
